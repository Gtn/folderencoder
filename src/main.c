#include <math.h>
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <openssl/sha.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <zip.h>
#include <dirent.h>
#include <sys/stat.h>

#define BLOCK_SIZE 65536

int walk_dir(zip_t * zipper, zip_source_t * source, char * base_dir, int root_path_length);
void get_sha(unsigned char * sha, int sha_length, char * input);
void handle_ssl_errors(EVP_CIPHER_CTX * ctx);


int encrypt_file(char *file_path, char * key, char * iv);
int decrypt_file(char *file_path, char * key, char * iv);

int zip_dir(char *dir_path, char * output_path);
int unzip_dir(char *file_path, char * output_path);

void print_help();
void print_version();



int main (int argc, char** argv) {

	char directory_path[1024];
	directory_path[0] = 0;


	int option = -1; // 0 -> encrypt, 1 -> decrypt, -1 -> Not set
	// The end of the string will always be \0, and it need place for it. So it has 33 and not 32 characters
	unsigned char key[33] = "";
    unsigned char iv[17] = "";
	
	/*
	 * setup directory_path, key, iv and option
	 */

	if(argc < 6) {
		if(argc == 2) {
			if(strcmp(argv[1], "--version") == 0) {
				print_version();
				return 0;
			}
		}
		printf("Not enough argument, showing help message\n");
		print_help();
		return 0;
	} else {
		for(int i = 1; i < argc; i++) {
			if(argv[i][0] == '-') {
				switch(argv[i][1]) {
				case 'e':
					option = 0;
					break;
				case 'd':
					option = 1;
					break;
				case '-':
					if(argv[i][2] == 'h')
						print_help();
					else
						print_version();
					return 0;
				case 'k':
					if(i == argc - 1) {
						printf("No key specified\n");
						return 1;
					}
					get_sha(key, 32, argv[i + 1]);
					i++;
					break;
				case 'i':
					if(i == argc - 1) {
						printf("No IV specified\n");
						return 1;
					}
					get_sha(iv, 16, argv[i + 1]);
					i++;
					break;
				default:
					printf("Unknown parameter : -%c\n", argv[i][1]);
					return 1;
				}
			} else {
				// If the path isn't absolute
				if(directory_path[0] == 0) {
					if(argv[i][0] != '/') {
						char buf[256] = "";
						if(getcwd(buf, 256) == NULL) {
							printf("Can't access to working directory, please enter an absolute path\n");
						} 
						strcat(directory_path, buf);
						strcat(directory_path, "/");
					}
					strcat(directory_path, argv[i]);
				} else {
					printf("Folder or encrypted file specified mor ethan one time\n");
					return 1; 
				}
			}
		}
	}
	
	if(strcmp(key, "") == 0 || strcmp(iv, "") == 0) {
		printf("key or iv not specified\n");
		return 1;
	} 


	/*
	 * 
	 * SETUP ZIP_PATH
	 * 
	 */
	 
	char zip_path[2048] = "";
	strcpy(zip_path, directory_path);
	
	// remove the '/' at the end
	if(zip_path[strlen(zip_path) - 1] == '/')
		zip_path[strlen(zip_path) - 1] = 0;
	
	strcat(zip_path, ".tmp");
	
	// While the file already exist
	while( access( zip_path, F_OK ) != -1 ) {
		if(strlen(zip_path) < 2047) {
			strcat(zip_path, "0");
		} else {
			printf("Please make sure you can write in the upper directory of '%s', and that there is no file named '%s' followed by 0 or more '0'\n(Like %s or %s0 ...)\n", zip_path, zip_path, zip_path, zip_path);
			return 1;
		}
	}


	/* 
	 * make sure the path specified point to a path (decrypt) or a directory (encrypt)
	 */
	// Autmatic guess if encrypt or decrypt not specified
	
	if(option == -1) {
		// if the path doesn't end with '/', add it
		if(directory_path[strlen(directory_path) - 1] != '/') {
			strcat(directory_path, "/");
		} 
		// Then, we check, we add '/'. If we can access the folder pointed by directory_path, it's a dir
		if( access( directory_path, F_OK ) != -1 ) {
			option = 0;
		} else {
			// If we can access it without '/' but we can't with '/', it must be a file
			directory_path[strlen(directory_path) - 1] = 0;
			if( access( directory_path, F_OK ) != -1 ) {
				option = 1;
			} else {
				//file doesn't exist
				printf("'%s' doesn't exist, failed to guess encrypt or decrypt\n", directory_path);
				return 1;
			}
		}
	// If the option is set autmaticaly, no need to verify if the files exist and are the right type
	} else {
		if(option == 0) {
			// Make sure directory_path end with '/'
			if(directory_path[strlen(directory_path) - 1] != '/')
				strcat(directory_path, "/");

			// If we can access it, it's a folder
			if( access( directory_path, F_OK ) == -1 ) {
				printf("The '%s' folder doesn't exist...\n", directory_path);
				return 1;
			}
			
		} else {
			// Make sure directory_path doesn't end with '/'
			if(directory_path[strlen(directory_path) - 1] == '/')
				directory_path[strlen(directory_path) - 1] = 0;
			
			// If it doesn't exist, abort, else, check if it's a file
			if( access( directory_path, F_OK ) == -1 ) {
				printf("'%s' doesn't exist...\n", directory_path);
				return 1;
			} else {
				struct stat path_stat;
				stat(directory_path, &path_stat);
				// If it's a file and not a directory
				if( ! S_ISREG(path_stat.st_mode) ) {
					printf("'%s' is a directory, while it should be a file\n", directory_path);
					return 1;
				} 
			}
		}
	}

	
	if(option == 0) {
		if(zip_dir(directory_path, zip_path) != 0) {
			printf("Error while zipping your file, no data are compromised\nWARNING : NOTHING IS ENCRYPTED\n");
			return 1;
		}

		if(encrypt_file(zip_path, key, iv) != 0) {
			printf("Error while encrypting\nYour data are not compromised, but you have a zip copy of them in %sWARNING : NOTHING IS ENCRYPTED\n", zip_path);
			return 1;
		}
		
		char cmd[1024] = "/bin/wipe -rfs ";
		strcat(cmd, directory_path);
		if(system(cmd) == -1) {
			printf("Error while wiping folder : '%s' (error : %s)\nYout data shouldn't be compromised. You may want to delete your plain text directory, as your data are encrypted in '%s'\n", directory_path, strerror(errno),  zip_path);
			return 1;
		}
		// Remove the '/' character
		directory_path[strlen(directory_path) - 1 ] = 0;
		if(rename(zip_path, directory_path) == -1) {	
			printf("error while renaming file : %s\nYou must rename manually the file : '%s' into '%s'\n", strerror(errno), zip_path, directory_path);
			return 1;
		}
		printf("Data successfully encrypted\n");
		
	} else {		
		if( decrypt_file(directory_path, key, iv) != 0 ) {
			printf("Error while decrypting data, are you sure you remember your password ?\nYou may want to retry, but if it continue to fail, the data are lost\nWARNING : NOTHING IS DECRYPTED\n");
			return 1;
		}
		
		if(rename(directory_path, zip_path) == -1) {
			printf("Error while renaming file : '%s' in '%s' (%s). Please make sure you don't have another file named '%s'\n", directory_path, zip_path, strerror(errno), zip_path);
			return 1;
			
		}
		if(unzip_dir(zip_path, directory_path) != 0) {
			printf("Error while unzipping your data : %s\nYout data are not lost, they are just zipped in : '%s'\n", zip_path, zip_path);
			return 1;
		}
		if(remove(zip_path) == -1) {
			printf("Error while deleting zip archive : %s\nYout data has been successfuly decrypted, but you still have a zip archive of them in '%s'\n", strerror(errno), zip_path);
			return 1;
		}
		printf("Data successfully decrypted\n");
	}
}

int walk_dir(zip_t * zipper, zip_source_t * source, char * base_dir, int root_path_length) {
	
	int error = 0;
	DIR * dir_buffer;
	struct dirent *sub_file;
	char full_path_buffer[1024];
	char * subdir_buffer;
	
	// Open the directory
	dir_buffer = opendir(base_dir);
	
	// if it's really an existing directory
	if(dir_buffer != NULL) {
		// Handle all its files
		while((sub_file = readdir(dir_buffer)) != NULL) {
			if(strcmp(sub_file->d_name, ".") == 0 || strcmp(sub_file->d_name, "..") == 0)
				continue;
			// Clear full_path_buffer
			// Update it to match full path of current file
			memset(full_path_buffer, 0, 1024);
			strcat(full_path_buffer, base_dir);
			strcat(full_path_buffer, sub_file->d_name);
			
			// Subdir buffer is used to get location against the root of the .zip file
			// For instance, /home/user/dir/ is the dir to zip
			// For the file foo :
			// The full_path_buffer is /home/user/dir/foo
			// And the subdir_buffer is /dir/foo
			subdir_buffer = full_path_buffer + root_path_length - 1;
			
			switch(sub_file->d_type) {
				
			// If it's a file or a link
			case DT_REG:
			case DT_LNK:
				// Create a source from the file
				source = zip_source_file(zipper, full_path_buffer, 0, 0);
				if(source == NULL) {
					printf("Can't create source for file : %s\n", full_path_buffer);
					return 1;
				}
				
				
				// Add the source to the zip archive
				if (zip_file_add(zipper, subdir_buffer, source, ZIP_FL_ENC_UTF_8) < 0) {
					printf("Cant add file '%s'\nError : %s\n", full_path_buffer, zip_strerror(zipper));
					zip_source_free(source);
					return 1;
				}
				break;
			
			// If it's a directory
			case DT_DIR:
				// Add it to the zip archive
				if (zip_dir_add(zipper, subdir_buffer, ZIP_FL_ENC_UTF_8) < 0) {
					printf("Cant add directory %s\nError : %s\n", full_path_buffer, zip_strerror(zipper));
					zip_source_free(source);
					return 1;
				}
				// Add '/' at the end because it's a dir and not a file
				strcat(full_path_buffer, "/");

				
				// Then go walk through it
				// If error == 0 when living the last call of walk_dir, it means that there never was an error. Else, it means that [error] files failed 
				error += walk_dir(zipper, source, full_path_buffer, root_path_length);
				
				break;
				
			default:
				printf("The file %s isn't a directory, nor a file, nor a link. WTF is it ??", full_path_buffer);
				return 1;
				break;
			}
			
		}
		closedir(dir_buffer);
	} else {
		printf("Failed to walk through directory %s\n", base_dir);
		return 1;
	}
	return error;
}



int zip_dir(char *dir_path, char * outuput_path) {

	// Creating zipper
	int int_err;

	zip_t *zipper = zip_open(outuput_path, ZIP_CREATE | ZIP_EXCL, &int_err);
	
	if(zipper == NULL) {
		zip_error_t ziperror;
		zip_error_init_with_code(&ziperror, int_err);
		printf("Can't initialize zip archive : %s\n", zip_error_strerror(&ziperror));
		return 1;
	}
	
	zip_source_t * source;
	int err = walk_dir(zipper, source, dir_path, strlen(dir_path));
	
	if(err != 0) {
		zip_close(zipper);
		printf("%d files / directories encountered problems while encrypting. YOU MAY LOSE ALL DATA THEY CONTAINED IF YOU CONTINUE ! Do you still want to continue ?\n(y = yes / n = no, default = no) ", err);
		char choice = fgetc(stdin);
		if(strcmp(&choice, "y") == 0)
			return 0;
		else
			return 1;
	}
	zip_close(zipper);
	
	// Finishing
	return 0;
}

int make_dir(char * path) {
	if(path[strlen(path) - 1] != '/')
		strcat(path, "/");
		
	// If the first char is '/', it must ignore it. So it begin at 1, the second character
	for(int i = 1; i < strlen(path); i++) {
		if(path[i] != '/') 
			continue;
		// Replace the / with end of string
		path[i] = 0;
		
		// Check if it exist
		struct stat st = {0};
		if (stat(path, &st) == -1) {
			// If not, create the directory
			if(mkdir(path, 0700) != 0) {
				printf("Cannot create directory %s\n", path);
				path[i] = '/';
				return 1;
			}
		}
		// Reset the char
		path[i] = '/';
		// Continue
	}
	return 0;
}

int unzip_dir(char * file_path, char * output_path) {

	//check if output_path existn create it if not
	make_dir(output_path);
	
	// Creating zipper
	int int_err, nbr_of_files;
	zip_t *zipper = zip_open(file_path, ZIP_RDONLY , &int_err);
	if(zipper == NULL) {
		zip_error_t ziperror;
		zip_error_init_with_code(&ziperror, int_err);
		printf("Can't initialize zip archive : %s\n", zip_error_strerror(&ziperror));
		return 1;
	}
	
	// Get number of files
	nbr_of_files = zip_get_num_entries(zipper, 0);
	if(nbr_of_files < 0) {
		printf("Cannot read archive\n");
		return 1;
	}
	
	zip_file_t * file;
	char path_buffer[2048], name[1024];
	
	// Iterate in all files
	for(int i = 0; i < nbr_of_files; i++) {
		// Copy the name of file (accessing it directly is too dangerous, it can break things in case of crash
		strcpy(name, zip_get_name(zipper, i, ZIP_FL_ENC_GUESS));
		
		memset(path_buffer, 0, 2048);
		strcpy(path_buffer, output_path);
		strcat(path_buffer, name);
		
		
		// If it's a directory
		if(path_buffer[strlen(path_buffer) - 1] == '/') {
			//Make it
			make_dir(path_buffer);
		} else {
			// Check if the file already exist
			if( access( path_buffer, F_OK ) != -1 ) {
				// If it already exist, ask user if it overwrite it
				printf("File : '%s' already exist, overwrite it ?\n(y = yes, n = no (default = no) ", path_buffer);
				char overwrite_ask = fgetc(stdin);
				printf("\n");
				if(overwrite_ask != 'y') 
					continue;
			}
			
			file = zip_fopen_index(zipper, i, ZIP_FL_UNCHANGED);
			
			// Write data to file
			if(file == NULL) {
				printf("Archive is corrupted, cannot read data\nSorry, your data may be lost (you can try to repair the zip file)\n");
				return 1;
			}
			
			// Opening temporary file for writing encoded and unzipped file	
			FILE * output = fopen(path_buffer, "w");
			if( output == NULL ) {
				printf("Can't open ouput file '%s'\nContinuing anyway", path_buffer);
				continue;
			}
			
			int char_read;
			char buffer[65563];
			
			// Writing to it
			do {
				char_read = zip_fread( file, buffer, 65536);
				fwrite(buffer, char_read, 1, output);
			} while( char_read == 65536 );
			fclose(output);
			
		}
	}
//	
//	// Finishing
	zip_close(zipper);
	return 0;
}


int decrypt_file(char *file_path, char * key, char * iv) {
	FILE * zip_file = fopen(file_path, "rw+");
	
	if(zip_file == NULL) {
		printf("Can't open the partition, you may need superuser permission\n");
		return 1;
	}
	
	// Cipher is always [1 - 16] bytes larger to plain, its size is equals to the next 16 multiple
	// e.g. plain = 16 -> ciher = 32    //    plain = 37 -> cipher = 48
	// So with BLOCK_SIZE - 1 (2^16 - 1 = 65535), encrypted is 1 byte larger
	char plaintext[BLOCK_SIZE - 1], encrypted[BLOCK_SIZE];
	char to_append;
	unsigned long int complete_blocks;
	unsigned short int last_block_size;
	int len_tmp, plaintext_len = 0;
	EVP_CIPHER_CTX *ctx;

    if(!(ctx = EVP_CIPHER_CTX_new())) {
		handle_ssl_errors(ctx);
		return 1;
	}
	
	
	// Read last block size and number of complete blocks
	fseek(zip_file, -8, SEEK_END);
	fscanf(zip_file, "%hx", &last_block_size);

	fseek(zip_file, -24, SEEK_END);
	fscanf(zip_file, "%lx", &complete_blocks);
	

	fseek(zip_file, 0, SEEK_SET);
	
	for(int i = 0; i < complete_blocks; i++) {
		fseek(zip_file, i * (BLOCK_SIZE - 1), SEEK_SET);
		if( fread(encrypted, BLOCK_SIZE - 1, 1, zip_file) == EOF ) {
			printf("Failed to read zip_file (you may need superused rights)\n");
			return 1;
		}
		
		fseek(zip_file, complete_blocks * (BLOCK_SIZE - 1) + last_block_size + i, SEEK_SET);
		if( ( to_append = fgetc(zip_file) ) == EOF ) {
			printf("Failed to read zip_file (you may need superused rights)\n");
			return 1;
		}
		
		encrypted[BLOCK_SIZE - 1] = to_append;

		if(1 != EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, (unsigned char *) key, (unsigned char *) iv)) {
			handle_ssl_errors(ctx);
			return 1;
		} 
		
		//Decrypt the message
		if(1 != EVP_DecryptUpdate(ctx, (unsigned char *) plaintext, &len_tmp, (unsigned char *) encrypted, BLOCK_SIZE)) {
			handle_ssl_errors(ctx);
			return 1;
		}
		plaintext_len = len_tmp;

		// Finalise the decryption. 
		if(1 != EVP_DecryptFinal_ex(ctx, (unsigned char *) plaintext + len_tmp, &len_tmp)) {
			handle_ssl_errors(ctx);
			return 1;
		}
		plaintext_len += len_tmp;

		if(plaintext_len != BLOCK_SIZE - 1) {			
			printf("Error in decryption! Abort\n");
			return 1;
		}
		
		// Go to beginning of block
		fseek(zip_file, i * (BLOCK_SIZE - 1), SEEK_SET);
		// Owerwrite it
		fwrite(plaintext, BLOCK_SIZE - 1, 1, zip_file);

		/* Clean up */
		if(EVP_CIPHER_CTX_reset(ctx) == 0) { 
			handle_ssl_errors(ctx);
			return 1;
		}
	}
	
	if(last_block_size != 0) {
		fseek(zip_file, complete_blocks * (BLOCK_SIZE - 1), SEEK_SET);
		
		if( fread(encrypted, last_block_size, 1, zip_file) == EOF ) {
			printf("Failed to read zip_file (you may need superused rights)\n");
			return 1;
		}

		if(1 != EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, (unsigned char *) key, (unsigned char *) iv)) {
			handle_ssl_errors(ctx);
			return 1;
		} 
		// Decrypt the message
		if(1 != EVP_DecryptUpdate(ctx, (unsigned char *) plaintext, &len_tmp, (unsigned char *) encrypted, last_block_size)) {
			handle_ssl_errors(ctx);
			return 1;
		}
		
		plaintext_len = len_tmp;

		// Finalise the decryption. 
		if(1 != EVP_DecryptFinal_ex(ctx, (unsigned char *) plaintext + len_tmp, &len_tmp)) {
			handle_ssl_errors(ctx);
			return 1;
		}
		
		plaintext_len += len_tmp;
		
		// Go to beginning of block
		fseek(zip_file, complete_blocks * (BLOCK_SIZE - 1), SEEK_SET);
		// Owerwrite it
		fwrite(plaintext, plaintext_len, 1, zip_file);

		/* Clean up */
		if(EVP_CIPHER_CTX_reset(ctx) == 0) { 
			handle_ssl_errors(ctx);
			return 1;
		}
	}
	EVP_CIPHER_CTX_free(ctx);
	
	if(ftruncate(fileno(zip_file), complete_blocks * (BLOCK_SIZE - 1) + plaintext_len) != 0) {
		printf("Failed to resize the file, ERROR = %s, abort\n", strerror(errno));
		abort();
	}
	
	fclose(zip_file);
	
	return 0;
}

int encrypt_file(char *file_path, char * key, char * iv) {
	FILE * zip_file = fopen(file_path, "rw+");
	
	if(zip_file == NULL) {
		printf("Can't open the zip_file, you may need superuser permission\n");
		return 1;
	}
	
	// Cipher is always [1 - 16] bytes larger to plain, its size is equals to the next 16 multiple
	// e.g. plain = 16 -> ciher = 32    //    plain = 37 -> cipher = 48
	// So with BLOCK_SIZE - 1 (2^16 - 1 = 65535), encrypted is 1 byte larger
	char plaintext[BLOCK_SIZE - 1], encrypted[BLOCK_SIZE];
	unsigned long int file_size;
	unsigned long int complete_blocks;
	unsigned short int last_block_size;
	int len_tmp, cipher_len;
	
	EVP_CIPHER_CTX *ctx;
	
    /* Create and initialise the context */
    if(!(ctx = EVP_CIPHER_CTX_new())) { 
		handle_ssl_errors(ctx);
		return 1;
	} 
	
//		Get the size of the entire file
	fseek(zip_file, 0, SEEK_END);
	file_size = ftell(zip_file);
	fseek(zip_file, 0, SEEK_SET);
	
	// Calc number of entire blocks
	complete_blocks = floor( (double) file_size / (float) (BLOCK_SIZE - 1) );

	// Calc the size of last block
	
	// If right, there's no last block
	if( complete_blocks * (BLOCK_SIZE - 1) == file_size ) {
		last_block_size = 0;
	} else {
		float tmp = file_size - ((BLOCK_SIZE - 1) * complete_blocks);
		float j = 0;

		do {
			j++;
		}while ( (tmp + j) / 16 != floor ((tmp + j) / 16) );
		
		last_block_size = (unsigned short int) (tmp + j);
	}
	
	// Process all complete blocks
	for(long int i = 0; i < complete_blocks; i++) {
		fseek(zip_file, i * (BLOCK_SIZE - 1), SEEK_SET);
		
		if( fread(plaintext, BLOCK_SIZE - 1, 1, zip_file) == EOF ) {
			printf("Failed to read zip_file (you may need superused rights)\n");
			return i;
		}
		
		// Init the EVO encryption
		if(1 != EVP_EncryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, (unsigned char *) key, (unsigned char *) iv)) {
			handle_ssl_errors(ctx);
			return i;
		}
		
		// Encrypt the message
		if(1 != EVP_EncryptUpdate(ctx, (unsigned char *) encrypted, &len_tmp, (unsigned char *) plaintext, BLOCK_SIZE - 1)) {
			handle_ssl_errors(ctx);
			return i;
		}
			
		cipher_len = len_tmp;
		
		
		// Final encryption call
		if(1 != EVP_EncryptFinal_ex(ctx, (unsigned char *) encrypted + len_tmp, &len_tmp)) {
			handle_ssl_errors(ctx);
			return i;
		}
		
		cipher_len += len_tmp;
		
		if(cipher_len != BLOCK_SIZE) {
			printf("Cipher len = %d\n", cipher_len);
			printf("Error in encryption! Abort\n");
			return i;
		}

		// Overwrite the first BLOCK_SIZE - 1 = 65535 bytes
		fseek(zip_file, i * (BLOCK_SIZE - 1), SEEK_SET);
		fwrite(encrypted, BLOCK_SIZE - 1, 1, zip_file);
		
		// Write the last byte in the end of the file
		fseek(zip_file, complete_blocks * (BLOCK_SIZE - 1) + last_block_size + i, SEEK_SET);
		fputc(encrypted[BLOCK_SIZE - 1], zip_file);
		
		// Reset the contexts
		if(EVP_CIPHER_CTX_reset(ctx) == 0) { 
			handle_ssl_errors(ctx);
			return i;
		}
	}
	
	
	
	// Process last block, if there's one
	if(last_block_size != 0) {
		int decrypted_size = file_size - (complete_blocks * (BLOCK_SIZE - 1));
		
		// Go to the beginning of the last block
		fseek(zip_file, complete_blocks * (BLOCK_SIZE - 1), SEEK_SET);
		
		// Read the last block
		if( fread(plaintext, decrypted_size, 1, zip_file) == EOF ) {
			printf("Failed to read zip_file (you may need superused rights)\n");
			return complete_blocks;
		}
		
		
		// Init the EVO encryption
		if(1 != EVP_EncryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, (unsigned char *) key, (unsigned char *) iv)) {
			handle_ssl_errors(ctx);
			return complete_blocks;
		}
		
		// Encrypt the message
		if(1 != EVP_EncryptUpdate(ctx, (unsigned char *) encrypted, &len_tmp, (unsigned char *) plaintext, decrypted_size)) {
			handle_ssl_errors(ctx);
			return complete_blocks;
		}
			
		cipher_len = len_tmp;
		
		
		// Final encryption call
		if(1 != EVP_EncryptFinal_ex(ctx, (unsigned char *) encrypted + len_tmp, &len_tmp)) {
			handle_ssl_errors(ctx);
			return complete_blocks;
		}
		
		cipher_len += len_tmp;
		
		if(cipher_len != last_block_size) {
			printf("Cipher len = %d\n", cipher_len);
			printf("Error in encryption! Abort\n");
			return complete_blocks;
		}
		
		// Overwrite the last block
		fseek(zip_file, complete_blocks * (BLOCK_SIZE - 1), SEEK_SET);		
		fwrite(encrypted, last_block_size, 1, zip_file);		
	}
	
	// Go to the end
	fseek(zip_file, complete_blocks * (BLOCK_SIZE - 1) + last_block_size + complete_blocks, SEEK_SET);
	
	// Convert complete blocks to char *
	// write them at the end of the file
	char str_tmp[16];
	snprintf(str_tmp, 16, "%lx", complete_blocks);
	fwrite(str_tmp, 16, 1, zip_file);

	snprintf(str_tmp, 8, "%x", last_block_size);
	fwrite(str_tmp, 8, 1, zip_file);
	
	// Close the zip_file file
	fclose(zip_file);
	
	EVP_CIPHER_CTX_free(ctx);
	return 0;
}

void handle_ssl_errors(EVP_CIPHER_CTX * ctx) {
    printf("ERROR : ");
	ERR_print_errors_fp(stderr);
	if(ctx != NULL)
		EVP_CIPHER_CTX_free(ctx);
}



void get_sha(unsigned char * sha, int sha_length, char * input) {
	unsigned char sha_tmp[32];
	char sha_buf_hex[2];

	// Initialyzing the SHA256 converter
	SHA256_CTX context;
	if(!SHA256_Init(&context)) {
		 printf("Failed to init the SHA CTX, aborting");
	}

	// Accessing the sha_256 of the password
	SHA256_Update(&context,input, sizeof(input));
	SHA256_Final(sha_tmp, &context);

	for(int i = 0; i < sha_length / 2; i++) {
		// Convert the characters in two characters representing its ascii code
		sprintf(sha_buf_hex, "%x", sha_tmp[i]);
		
		// If ascii code < 16, add a 0 before, so we have 0d for instance, and not just d
		if(strlen(sha_buf_hex) == 1) 			
			strcat(sha, "0");
			
		// Add the two hexa characters to the key
		strcat(sha, sha_buf_hex);				
	}
	
	// Free sensitive data
	memset(sha_tmp, 0, 32);
	memset(sha_buf_hex, 0, 2);
	return;
}



void print_help() {
	printf("\
Usage : fencode [OPTION] [FOLDER | ENCRYPTED FILE]\n\
Options : \n\
  -e        Encrypt a Folder (can be omitted, it will automaticaly set -g option)\n\
  -d        Decrypt a File (can be omitted, it will automaticaly set -g option) \n\
  -g        Guess encryption or decryption (default)\n\
  -k [key]  Specify the key\n\
  -i [iv]   Specify the IV\n\
  --help    Show this message\n\n\
By Gétéhèn\n"    );
}

void print_version() {
	printf("\
Version = 1.0\n\n\
Coded in C by Gétéhèn\n\
Source code is available at : www.framagit.org/Gtn/folderencoder\n\n\
Libraries used \n\
- openssl (libssl)\n\
- libzip\n"    );
}