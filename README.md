# FolderEncoder

Simple folder zipper and encoder (aes-256 encryption algorithm)

## Dependencies

wipe (https://linux.die.net/man/1/wipe)

Installation : 

### Debian and derivates (Ubuntu, Mint)
```
sudo apt install wipe
```

### Arch and derivates (Manjaro...)
```
sudo pacman -S wipe
```

### Fedora / Centos
```
sudo dnf install wipe
```

### Redhat
```
sudo yum -y install wipe
```

## Usage

```
Usage : fencode [OPTION] [FOLDER | ENCRYPTED FILE]
Options : 
  -e        Encrypt a Folder (can be omitted, it will automaticaly set -g option)
  -d        Decrypt a File (can be omitted, it will automaticaly set -g option) 
  -g        Guess encryption or decryption (default)
  -k [key]  Specify the key
  -i [iv]   Specify the IV
  --help    Show this message
  --version Show credits, version, and libraries used

By Gétéhèn
```

## Credits

Version = 1.0

Language : C

Libraries used 
- openssl (libssl)
- libzip

## DISCLAIMER

See in LICENSE, paragraph fifteen (15)

A copy is available here : 

THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY
APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT
HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY
OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM
IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF
ALL NECESSARY SERVICING, REPAIR OR CORRECTION.

